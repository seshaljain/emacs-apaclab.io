HUGO ?= hugo
FILE_NAME ?= $(shell date "+%B-%Y" | tr '[:upper:]' '[:lower:]').md
ANN_DIR = content/announcements

build:
	$(HUGO) --gc --minify --buildExpired

announcement:
	@echo "Creating announcement file in '$(ANN_DIR)'."
	mkdir --parents $(ANN_DIR)
	$(HUGO) new $(ANN_DIR)/$(FILE_NAME) --kind "announcement"

# TODO(bhavin192): add note target

server:
	$(HUGO) server --buildDrafts --buildFuture --buildExpired
