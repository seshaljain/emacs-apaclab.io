---
title: "Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, December 26, 2020"
linktitle: "December 2020 virtual meetup"
date: 2020-12-20T22:59:50+05:30
expirydate: 2020-12-26T14:00:00+05:30
categories:
- Event
---

This month's [Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io)
virtual meetup is scheduled for Saturday, December 26, 2020 at [1400
IST](# "02:00 PM Indian Standard Time (UTC+05:30)") (~1h) with Jitsi
Meet and `#emacs` on Freenode IRC.

### Talks
- **Simple lsp for C by mohan43u (~20m)**  
  How-to setup lsp-mode, lsp-ui, flycheck with clangd to setup c/c++
  development environment in Emacs.

If you would like to give a demo or talk (maximum 20 minutes) on GNU
Emacs or any variant, please contact `bhavin192` on Freenode with your
talk details:

- Topic
- Description
- Duration
- About Yourself

The Jitsi Meet (video conferencing) URL for the session will be posted
on Freenode IRC channels `#emacs`, `#ilugc` and `#emacsconf`, 30
minutes prior to the meeting, and also on the [ILUGC mailing
list](https://www.freelists.org/list/ilugc) on the day of the
meetup. If you are not subscribed, you can also check the
[archive](https://www.freelists.org/archive/ilugc/).
